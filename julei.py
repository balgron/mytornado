import pandas as pd
import numpy as np
from pandas import DataFrame,Series
from factor_analyzer import FactorAnalyzer, Rotator


#构建数据集
mydata = {
       '进球':[16,12,16,8],
       '进球转化率':[19,14,13,10],
       '助攻':[8,2,7,4],
}
data = DataFrame(mydata)

data.index=['苏亚雷斯','莫拉塔','哲科','伊瓜因']

# print(data)

# print(np.linalg.matrix_rank(data))


fa = FactorAnalyzer(rotation=None)
fa.fit(data)

# print(fa.loadings_)

rotator = Rotator()
print("旋转后矩阵:\n", rotator.fit_transform(fa.loadings_))



print(fa.get_communalities())

def F(factors):
    return sum(factors*fa.get_factor_variance()[1])

scores = []
for i in range(len(fa.transform(data))):
    new = F(fa.transform(data)[i])
    scores.append(new)

print(data)


data['综合打分'] = scores

print(data)

data = data.sort_values(by='综合打分',ascending=False)

print(data)


