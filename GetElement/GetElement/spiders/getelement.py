# -*- coding: utf-8 -*-
from selenium import webdriver
import scrapy
import json
import time


class GetelementSpider(scrapy.Spider):
    name = 'getelement'
    allowed_domains = ['elements.envato.com']
    start_urls = ['https://elements.envato.com/web-templates']

    base_url = 'https://elements.envato.com'
    base_page = 'https://elements.envato.com/web-templates/pg-'
    totalPages = 50
    currentPage = 2
    

    # 调用cookie
    str = ''
    with open('elements.json', 'r', encoding='utf-8') as f:
        listCookies = json.loads(f.read())
    cookie = [item["name"] + "=" + item["value"] for item in listCookies]
    cookiestr = '; '.join(item for item in cookie)

    
    headers = {
        'cookie': cookiestr,
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'
    }



    def parse(self, response):
        

        # 对一页的内容进行解析
        li_list = response.xpath('/html').extract_first()
        jsonCookies = json.dumps(li_list)
        with open('elements_html.txt', 'w') as f:
            f.write(jsonCookies)
        print(li_list)
        
        

        