# import nltk
# import ssl
# from nltk.stem.lancaster import LancasterStemmer
# stemmer = LancasterStemmer()

# import numpy as np
# from keras.models import Sequential
# from keras.layers import Dense, Activation, Dropout
# from keras.optimizers import SGD
# import pandas as pd
# import pickle
# import random


# words = []
# classes = []
# documents = []
# ignore_words = ['?']
# # loop through each sentence in our intents patterns


# intents = {"intents": [
#         {"tag": "打招呼",
#          "patterns": ["你好", "您好", "请问", "有人吗", "师傅","不好意思","美女","帅哥","靓妹"],
#          "responses": ["您好", "又是您啊", "吃了么您内","您有事吗"],
#          "context": [""]
#         },
#         {"tag": "告别",
#          "patterns": ["再见", "拜拜", "88", "回见", "回头见"],
#          "responses": ["再见", "一路顺风", "下次见", "拜拜了您内"],
#          "context": [""]
#         },
#    ]
# }

# for intent in intents['intents']:
#     for pattern in intent['patterns']:
#         # tokenize each word in the sentence
#         w = nltk.word_tokenize(pattern)
#         # add to our words list
#         words.extend(w)
#         # add to documents in our corpus
#         documents.append((w, intent['tag']))
#         # add to our classes list
#         if intent['tag'] not in classes:
#             classes.append(intent['tag'])
# # stem and lower each word and remove duplicates
# words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
# words = sorted(list(set(words)))
# # sort classes
# classes = sorted(list(set(classes)))
# # documents = combination between patterns and intents
# # print (len(documents), "documents")
# # # classes = intents
# # print (len(classes), "语境", classes)
# # # words = all words, vocabulary
# # print (len(words), "词数", words)


# # create our training data
# training = []
# # create an empty array for our output
# output_empty = [0] * len(classes)
# # training set, bag of words for each sentence
# for doc in documents:
#     # initialize our bag of words
#     bag = []

#     pattern_words = doc[0]
   
#     pattern_words = [stemmer.stem(word.lower()) for word in pattern_words]

#     for w in words:
#         bag.append(1) if w in pattern_words else bag.append(0)
    
 
#     output_row = list(output_empty)
#     output_row[classes.index(doc[1])] = 1
    
#     training.append([bag, output_row])

# random.shuffle(training)
# training = np.array(training)

# train_x = list(training[:,0])
# train_y = list(training[:,1])


# model = Sequential()
# model.add(Dense(128, input_shape=(len(train_x[0]),), activation='relu'))
# model.add(Dropout(0.5))
# model.add(Dense(64, activation='relu'))
# model.add(Dropout(0.5))
# model.add(Dense(len(train_y[0]), activation='softmax'))


# sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
# model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])


# model.fit(np.array(train_x), np.array(train_y), epochs=200, batch_size=5, verbose=1)


# # json_file = model.to_json()
# # with open('v3ucn.json', "w") as file:
# #    file.write(json_file)

# # model.save_weights('./v3ucn.h5f')


# from keras.models import model_from_json
# # # load json and create model
# file = open("./v3ucn.json", 'r')
# model_json = file.read()
# file.close()
# model = model_from_json(model_json)
# model.load_weights("./v3ucn.h5f")



# def clean_up_sentence(sentence):
#     # tokenize the pattern - split words into array
#     sentence_words = nltk.word_tokenize(sentence)
#     # stem each word - create short form for word
#     sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
#     return sentence_words
# # return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
# def bow(sentence, words, show_details=True):
#     # tokenize the pattern
#     sentence_words = clean_up_sentence(sentence)
#     # bag of words - matrix of N words, vocabulary matrix
#     bag = [0]*len(words)  
#     for s in sentence_words:
#         for i,w in enumerate(words):
#             if w == s: 
#                 # assign 1 if current word is in the vocabulary position
#                 bag[i] = 1
#                 if show_details:
#                     print ("found in bag: %s" % w)
#     return(np.array(bag))


# def classify_local(sentence):
#     ERROR_THRESHOLD = 0.25
    
#     # generate probabilities from the model
#     input_data = pd.DataFrame([bow(sentence, words)], dtype=float, index=['input'])
#     results = model.predict([input_data])[0]
#     # filter out predictions below a threshold, and provide intent index
#     results = [[i,r] for i,r in enumerate(results) if r>ERROR_THRESHOLD]
#     # sort by strength of probability
#     results.sort(key=lambda x: x[1], reverse=True)
#     return_list = []
#     for r in results:
#         return_list.append((classes[r[0]], str(r[1])))
#     # return tuple of intent and probability
    
#     return return_list


# # p = bow("你好", words)
# # print (p)

# print(classify_local('请问'))

# import urllib.parse
# import requests

# def qingyunke(msg):
#     url = 'http://api.qingyunke.com/api.php?key=free&appid=0&msg={}'.format(urllib.parse.quote(msg))
#     html = requests.get(url)
#     return html.json()["content"]
# msg = '吃了么'
# print("原话>>", msg)
# res = qingyunke(msg)
# print("青云客>>", res)


# feibonacci = lambda n : 1 if n <= 2 else feibonacci(n-1)+feibonacci(n-2)

# for x in range(10):
#     print(feibonacci(x))

# import sys
# #更改递归深度为1百万
# sys.setrecursionlimit(1000000)

# def sum_number(n):
#     if n <= 0:
#         return 0
#     return n+sum_number(n-1)


# print(sum_number(1500))

class Tree:
    def __init__(self):
        self.left = None
        self.right = None
        self.data = None


root = Tree()
root.data = "root"
root.left = Tree()
root.left.data = "left"
root.right = Tree()
root.right.data = "right"

root.left.left = Tree()
root.left.left.data = 'leftleft'