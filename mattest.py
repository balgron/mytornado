import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams['font.sans-serif'] = ['SimHei']
matplotlib.rcParams['axes.unicode_minus'] = False

scores = [0.7294004536510521, -0.2958329655707666, 0.530110265958429, -0.9636777540387146]

plt.barh(range(4), scores, height=0.7, color='steelblue', alpha=0.8)  
plt.yticks(range(4), ['苏亚雷斯','莫拉塔','哲科','伊瓜因'])
plt.xlim(-1,2)
plt.xlabel("分数")
plt.title("引援打分")
for x, y in enumerate(scores):
    plt.text(y + 0.2, x - 0.1, '%s' % y)
plt.show()