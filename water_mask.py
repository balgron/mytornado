

# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont
 
# 指定要使用的字体和大小；/Library/Fonts/是macOS字体目录；Linux的字体目录是/usr/share/fonts/
 
# image: 图片  text：要添加的文本 font：字体
def add_text_to_image(image, text):
  rgba_image = image.convert('RGBA')
  text_overlay = Image.new('RGBA', rgba_image.size, (255, 255, 255, 0))
  image_draw = ImageDraw.Draw(text_overlay)
 
  text_size_x, text_size_y = image_draw.textsize(text)
  # 设置文本文字位置
  print(rgba_image)
  text_xy = (rgba_image.size[0] - text_size_x, rgba_image.size[1] - text_size_y)
  # 设置文本颜色和透明度
  image_draw.text(text_xy,text,fill=(76, 234, 124, 180))
 
  image_with_text = Image.alpha_composite(rgba_image, text_overlay)
 
  return image_with_text
 
im_before = Image.open("touxiang.png")
im_after = add_text_to_image(im_before, '1906a')
im_after.show()
im.save('im_after.jpg')



# from PIL import Image, ImageFont, ImageDraw # 导入模块
# im = Image.open("touxiang.png") # 打开文件
# print(im.format, im.size, im.mode)
# draw = ImageDraw.Draw(im) #修改图片

# #font = ImageFont.truetype(None, size = 40)#"C:\Users\Administrator\Desktop\每天小程序homework_day\0000\Helvetica Bold.ttf", 36) #更改文字字体

# draw.text((0,0), '1906a', fill = (76, 234, 124, 180)) #利用ImageDraw的内置函数，在图片上写入文字
# im.show()
# im.save('touxiang1.jpg')
