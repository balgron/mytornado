# import time

# all_apples = [1,1,1,1,1]

# def take_apples(num):
#     count = 0
#     while True:
#         if len(all_apples) == 0:
#             time.sleep(.1)
#         else:
#             apple = all_apples.pop()
#             yield apple
#             count += 1
#             if count == num:
#                 break

# def buy_apples():
#     bucket = []
#     for p in take_apples(50):
#         print(bucket)
#         bucket.append(p)


# print(buy_apples())


# async def ask_for_apple():
#     await asyncio.sleep(1)
#     all_apples.append(1)

# async def take_apples(num):
#     count = 0
#     while True:
#         if len(all_apples) == 0:
#             await ask_for_apple()
#         apple = all_apples.pop()
#         yield apple
#         count += 1
#         if count == num:
#             break

# async def buy_apples():
#     bucket = []
#     async for p in take_apples(10):
#         bucket.append(p)
#         print(bucket)

# import asyncio
# # loop = asyncio.get_event_loop()
# # res = loop.run_until_complete(buy_apples())
# # loop.close()


# async def async_function():
#     return 1

# print(async_function())


# async def async_function():
#     return 1

# async def await_coroutine():
#     result = await async_function()
#     print(result)

# asyncio.run(await_coroutine())



import nltk
nltk.download()