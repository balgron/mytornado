import requests

response = requests.get("http://www.google.com", proxies={
    'http': 'http://127.0.0.1:4780',
    'https': 'https://127.0.0.1:4780'
})

print(response.text)