# # @Time    : 2020-03-12-0012 21:11
# # @Author  : V万能的小黑V
# # @File    : bd_image.py

# # 从Python SDK导入BOS配置管理模块以及安全认证模块
# from baidubce.bce_client_configuration import BceClientConfiguration
# from baidubce.auth.bce_credentials import BceCredentials
# # 导入BOS相关模块
# from baidubce.services.bos.bos_client import BosClient


# # class Bd_Storage(object):
# #     def __init__(self):
# #         # 设置BosClient的Host，Access Key ID和Secret Access Key
# #         self.bos_host = "bj.bcebos.com"	# 地址可以改，参考百度的python SDK文档
# #         self.access_key_id = "5220a750c813486a97ff2739d40a727e"
# #         self.secret_access_key = "0e6956fef0ea441e8d7d167c7dd56ba5"
# #         self.back_name = "v3ucn"

# #     def up_image(self, key_name, file):
# #         config = BceClientConfiguration(credentials =
# #                                         BceCredentials(self.access_key_id, self.secret_access_key),
# #                                         endpoint = self.bos_host)
# #         client = BosClient(config)

# #         key_name = key_name
# #         try:
# #             res = client.put_object_from_string(bucket = self.back_name, key = key_name, data = file)
# #         except Exception as e:
# #             return None
# #         else:
# #             result = res.__dict__
# #             if result['metadata']:
# #                 # print("图片上传成功:{}".format("https://" + self.back_name + ".bj.bcebos.com/" + key_name))
# #                 url = 'https://' + self.back_name + '.bj.bcebos.com/' + key_name
# #                 print("图片上传成功:{}".format(url))


# # # 测试
# # with open('/Users/liuyue/Library/Containers/com.tencent.xinWeChat/Data/Library/Application Support/com.tencent.xinWeChat/2.0b4.0.9/42d7ed0f3956bc57a82f33e798e1d320/Message/MessageTemp/2529310e3fcfc6d65eb820cf53b7e4c9/File/众鲸.m4a', 'rb') as f:
# #     bd = Bd_Storage()
# #     s = f.read()
# #     bd.up_image("众鲸.m4a", s)

# import requests
# import json
# res = requests.get("https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=UEQ5IyEVImcHF40xSBqS0qVl&client_secret=lLZV7wyZeUmlgXImTgEvu08sGroDVRO8")

# res = json.loads(str(res.text))
# token = res['access_token']
# print(token)

# #创建
# url = 'https://aip.baidubce.com/rpc/2.0/aasr/v1/create'  #创建音频转写任务请求地址

# body = {
#     "speech_url": 'https://v3ucn.bj.bcebos.com/%E4%BC%97%E9%B2%B8.m4a',
#     "format": "m4a",        #音频格式，支持pcm,wav,mp3，音频格式转化可通过开源ffmpeg工具（https://ai.baidu.com/ai-doc/SPEECH/7k38lxpwf）或音频处理软件
#     "pid": 1537,        #模型pid，1537为普通话输入法模型，1737为英语模型
#     "rate": 16000       #音频采样率，支持16000采样率，音频格式转化可通过开源ffmpeg工具（https://ai.baidu.com/ai-doc/SPEECH/7k38lxpwf）或音频处理软件
# }
# mytoken = {"access_token":token}

# headers = {'content-type': "application/json"}

# print(mytoken)

# response = requests.post(url,params=mytoken,data = json.dumps(body), headers = headers)

# print(json.dumps(response.json(), ensure_ascii=False))

# res = response.json()

# print(token)



# temp_url = 'https://aip.baidubce.com/rpc/2.0/aasr/v1/query?access_token=' + token
# temp_headers = {'Content-Type': 'application/json'}
# body = {
#     "task_ids": [res['task_id']]
# }
# temp_res = requests.post(url=temp_url, data=json.dumps(body), headers=temp_headers)
# print(temp_res.json())


class A:
    def spam(self):
        print('A.spam')


class B(A):
    def spam(self):
        print('B.spam')
        super().spam()  # Call parent spam()


if __name__ == '__main__':
    b = B()
    b.spam()