# content = []
# with open('./excel.txt') as fp:  
#     content = fp.readlines()

# for val in content:
#     print(val)
#     with open('excel_back.txt', 'a+') as f:
#         f.write('%s\n' % val.replace("/",".")[0:7].rstrip(".") )


# class MyType(type):
#     print('执行__call__')
#     def __call__(cls, *args, **kwargs):
#         print('执行__new__')
#         obj = cls.__new__(cls,*args,**kwargs)

#         print('执行__init__')
#         obj.__init__('127.0.0.1')

#         return obj

# class mysql(metaclass=MyType):
#     def __init__(self,host):
#         self.host = host

#     def connect(self):
#         print('连接数据库{host}'.format(host=self.host))

# db = mysql('127.0.0.1')
# db.connect()





# def binary_search(li,key,start,end):

#     mid = (start+end)//2
#     if li[mid]==key:
#         return mid
#     if start>end:
#         return False
#     elif li[mid]<key:
#         return binary_search(li,key,mid+1,end)
#     else:
#         return binary_search(li,key,start,mid-1)


# print(binary_search([1,2,3],3,0,3))

# user = 0b010

# def my_decorator(func):
#     def wrapper(*args):
#         print(args[0])
#         func(*args)
 
#     return wrapper

# @my_decorator
# def foo(name):
#     print(444)
#     pass



# foo('123')


# a = [1, 2, 3, 1, 1, 2]
# dict = {}
# for key in a:
#     dict[key] = dict.get(key, 0) + 1


# dict = sorted(dict.items(), key=lambda item:item[1], reverse=True)

# print(dict)

# a = 1
# b = 2

# res = a if a > b else b

# print(res)


# def binary_search(lis, nun):
#     left = 0
#     right = len(lis) - 1
#     while left <= right:   #循环条件
#         mid = (left + right) // 2   #获取中间位置，数字的索引（序列前提是有序的）
#         if num < lis[mid]:  #如果查询数字比中间数字小，那就去二分后的左边找，
#             right = mid - 1   #来到左边后，需要将右变的边界换为mid-1
#         elif num > lis[mid]:   #如果查询数字比中间数字大，那么去二分后的右边找
#             left = mid + 1    #来到右边后，需要将左边的边界换为mid+1
#         else:
#             return mid  #如果查询数字刚好为中间值，返回该值得索引
#     return -1  #如果循环结束，左边大于了右边，代表没有找到
# ss = [1,2,3]
# print(binary_search(ss,3)


import redis

pool = redis.ConnectionPool()
conn = redis.Redis(connection_pool=pool)
# print(conn.set("x1",'x1'))
# print(conn.get("x1"))


class MyRedis:
    def __init__(self,key,**redis_kwargs):
       # redis的默认参数为：host='localhost', port=6379, db=0， 其中db为定义redis database的数量
       self.__db= redis.Redis(**redis_kwargs)
       self.key = '%s' % key

    def set(self,value):
        self.__db.set(self.key,value)

    def get(self):

        return self.__db.get(self.key)


# config = {'host':'127.0.0.1','port':6379}

# r = MyRedis('123',**config)

# r.set('444')

# print(r.get())

# mykeys = conn.keys('*')

# mykeys = [str(x,'utf-8') for x in mykeys]

# conn.delete('x1')

# print(mykeys)

def sum_number(n):
    if n <= 0:
        return 0
    sum = n+sum_number(n-1)
    print(sum)
    return sum

sum_number(5)


