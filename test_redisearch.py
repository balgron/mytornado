from redisearch import Client, TextField
# Creating a client with a given index name
client = Client('myIndex',host='localhost',port='6666')

#client.drop_index()

# Creating the index definition and schema
#client.create_index((TextField('title'), TextField('body')))

# Indexing a document
#client.add_document('doc2', title = '你好', body = '我在北京学习人工智能',language='chinese')

#client.add_document('doc3', title = '你好123', body = '我在北京学习人工智能',language='chinese')

# Simple search
res = client.search("人工智能")

print(res.docs[0].title)

for xy in res.docs:
    print(xy.title)