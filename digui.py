import sys
#更改递归深度为1百万
#sys.setrecursionlimit(1000000)



def sum_number(n):
    if n <= 0:
        return 0
    return n+sum_number(n-1)


def tail_sum(n,result=0):
     if n==0:
         return result
     else:
         return tail_sum(n-1, result+n) 


# def sum_number(n,result):
#     if n <= 0:
#         return result
#     return n+sum_number(n-1,result+n)

print(sum_number(5))

print(tail_sum(5))